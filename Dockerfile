FROM thingsboard/application:1.3.0

ENV MQTT_BIND_ADDRESS=0.0.0.0
ENV MQTT_BIND_PORT=1883
ENV COAP_BIND_ADDRESS=0.0.0.0
ENV COAP_BIND_PORT=5683
ENV ZOOKEEPER_URL=zk:2181
ENV DATABASE_TYPE=sql
ENV CASSANDRA_URL=cassandra:9042
ENV CASSANDRA_HOST=cassandra
ENV CASSANDRA_PORT=9042
ENV POSTGRES_HOST=postgres
ENV POSTGRES_PORT=5432
ENV ADD_SCHEMA_AND_SYSTEM_DATA=true

RUN mkdir -p /usr/share/thingsboard/conf /var/log/thingsboard && \
    chmod -R g+ws /var/run /var/log /usr/share/thingsboard/conf && \
    chown -R 1001:0 /var/run /var/log /usr/share/thingsboard/conf

RUN dpkg -D5 -i /thingsboard.deb
RUN /usr/share/thingsboard/bin/install/install.sh
RUN cp /run-application.sh /run-application-fast.sh
RUN sed -i '$ d' /run-application-fast.sh
RUN /run-application-fast.sh && sleep 30

CMD ["/run-application.sh"]

EXPOSE 8080
EXPOSE 1883
